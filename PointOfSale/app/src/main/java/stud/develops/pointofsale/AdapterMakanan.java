package stud.develops.pointofsale;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterMakanan extends RecyclerView.Adapter<AdapterMakanan.MakananViewHolder> {
    private ArrayList<Makanan>dataList;

    public AdapterMakanan(ArrayList<Makanan> dataList){
        this.dataList = dataList;
    }

    @Override
    public MakananViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item, parent, false);
        return new MakananViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MakananViewHolder holder, int position){
        holder.txt_nama.setText(dataList.get(position).getNama());
        holder.txt_harga.setText(dataList.get(position).getHarga());
        holder.txt_jumlah.setText(dataList.get(position).getJumlah());
    }

    @Override
    public int getItemCount(){
        return  (dataList != null) ? dataList.size():0;
    }

    public class MakananViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_nama, txt_harga, txt_jumlah;

        public MakananViewHolder (View itemView){
            super(itemView);
            txt_nama = (TextView) itemView.findViewById(R.id.txt_nama);
            txt_harga = (TextView) itemView.findViewById(R.id.txt_harga);
            txt_jumlah = (TextView) itemView.findViewById(R.id.txt_jumlah);
        }
    }
}
