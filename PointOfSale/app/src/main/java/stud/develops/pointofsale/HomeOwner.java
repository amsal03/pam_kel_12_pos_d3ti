package stud.develops.pointofsale;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class HomeOwner extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_owner);
    }

    public void profi(View view) {
        Intent intent = new Intent(HomeOwner.this, Profil.class);
        startActivity(intent);
    }

    public void menumakanan(View view) {
        Intent intent = new Intent(HomeOwner.this, TambahMakanan.class);
        startActivity(intent);
    }

    public void transaksi(View view) {
        Intent intent = new Intent(HomeOwner.this, Kasir.class);
        startActivity(intent);
    }

    public void laporan(View view) {
        Intent intent = new Intent(HomeOwner.this, Laporan.class);
        startActivity(intent);
    }

    public void pegawai(View view) {
        Intent intent = new Intent(HomeOwner.this, TampilData2.class);
        startActivity(intent);
    }

    public void kasir(View view) {
        Intent intent = new Intent(HomeOwner.this, Transaksi.class);
        startActivity(intent);
    }
}
