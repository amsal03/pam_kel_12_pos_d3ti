package stud.develops.pointofsale.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import stud.develops.pointofsale.R;
import stud.develops.pointofsale.TambahMakanan;
import stud.develops.pointofsale.model.DataModel;


/**
 * Created by ASUS-PC on 14/05/2019.
 */

public class AdapterData extends RecyclerView.Adapter<AdapterData.HolderData> {

    private List<DataModel> mList ;
    private Context ctx;


    public  AdapterData (Context ctx, List<DataModel> mList)
    {
        this.ctx = ctx;
        this.mList = mList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        DataModel dm = mList.get(position);
        holder.nama.setText(dm.getNama());
        holder.harga.setText(dm.getHarga());
        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends  RecyclerView.ViewHolder{
        TextView nama, harga;
        DataModel dm;
        public HolderData (View v)
        {
            super(v);

            nama  = (TextView) v.findViewById(R.id.tvNama);
            harga = (TextView) v.findViewById(R.id.tvHarga);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx, TambahMakanan.class);
//                    goInput.putExtra("id_makanan", dm.getId_makanan());
                    goInput.putExtra("nama", dm.getNama());
                    goInput.putExtra("harga", dm.getHarga());


                    ctx.startActivity(goInput);
                }
            });
        }
    }
}
