package stud.develops.pointofsale.api;



import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import stud.develops.pointofsale.model.ResponsModel;
import stud.develops.pointofsale.model.ResponsModel2;


/**
 * Created by ASUS-PC on 14/05/2019.
 */

public interface ApiRequest2 {
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponsModel2> sendBiodata(@Field("nama") String nama,
                                   @Field("username") String username,
                                    @Field("password") String password,
                                    @Field("jenis_kelamin") String jenis_kelamin,
                                    @Field("alamat") String alamat);



    @GET("read.php")
    Call<ResponsModel2> getBiodata();

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponsModel2> updateData(@Field("id_pegawai") String id_pegawai,
                                  @Field("nama") String nama,
                                  @Field("username") String username,
                                  @Field("password") String password,
                                  @Field("jenis_kelamin") String jenis_kelamin,
                                  @Field("alamat") String alamat);

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponsModel2> deleteData(@Field("id_pegawai") String id_pegawai);
}
