package stud.develops.pointofsale;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import stud.develops.pointofsale.adapter.AdapterData;
import stud.develops.pointofsale.adapter.AdapterData2;
import stud.develops.pointofsale.api.ApiRequest;
import stud.develops.pointofsale.api.ApiRequest2;
import stud.develops.pointofsale.api.RetroServer;
import stud.develops.pointofsale.api.RetroServer2;
import stud.develops.pointofsale.model.DataModel;
import stud.develops.pointofsale.model.DataModel2;
import stud.develops.pointofsale.model.ResponsModel;
import stud.develops.pointofsale.model.ResponsModel2;

public class TampilData2 extends AppCompatActivity {
    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mManager;
    private List<DataModel2> mItems = new ArrayList<>();
    ProgressDialog pd;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_data_pegawai);

        fab=(FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TampilData2.this, TambahPegawai.class);
                startActivity(intent);
            }
        });


        //pd = new ProgressDialog(this);
        mRecycler = (RecyclerView) findViewById(R.id.recyclerTemp);
        mManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(mManager);

//        pd.setMessage("Loading ...");
//        pd.setCancelable(false);
//        pd.show();

        ApiRequest2 api = RetroServer2.getClient().create(ApiRequest2.class);
        Call<ResponsModel2> getdata = api.getBiodata();
        getdata.enqueue(new Callback<ResponsModel2>() {
            @Override
            public void onResponse(Call<ResponsModel2> call, Response<ResponsModel2> response) {
                //pd.hide();
                Log.d("RETRO", "RESPONSE : " + response.body().getKode());
                mItems = response.body().getResult();

                mAdapter = new AdapterData2(TampilData2.this,mItems);
                mRecycler.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<ResponsModel2> call, Throwable t) {
                //pd.hide();
                Log.d("RETRO", "FAILED : respon gagal");

            }
        });

    }
}
