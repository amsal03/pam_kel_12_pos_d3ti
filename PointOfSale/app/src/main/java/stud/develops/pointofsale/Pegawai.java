package stud.develops.pointofsale;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class Pegawai extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdapterPegawai adapterPegawai;
    private ArrayList<Pegawaim> pegawaimArrayList;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pegawai);

        fab=(FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pegawai.this, TambahPegawai.class);
                startActivity(intent);
            }
        });

        addData();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        adapterPegawai = new AdapterPegawai(pegawaimArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Pegawai.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterPegawai);
    }

    void addData(){
        pegawaimArrayList = new ArrayList<>();
        pegawaimArrayList.add(new Pegawaim("Raissa", "Raissa", "if317015", "perempuan", "parapat"));
        pegawaimArrayList.add(new Pegawaim("Amsal", "Amsal", "if317035", "lakilaki", "tarutung"));
        pegawaimArrayList.add(new Pegawaim("Sweta", "Sweta", "if317007", "perempuan", "tarutung"));
        pegawaimArrayList.add(new Pegawaim("Kornel", "Kornel", "if317040", "lakilaki", "medan"));
        pegawaimArrayList.add(new Pegawaim("Miranda", "Miranda", "if317015", "perempuan", "parapat"));
    }
}
