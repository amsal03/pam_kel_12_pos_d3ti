package stud.develops.pointofsale;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import stud.develops.pointofsale.api.ApiRequest2;
import stud.develops.pointofsale.api.RetroServer2;
import stud.develops.pointofsale.model.ResponsModel2;

public class TambahPegawai extends AppCompatActivity {
    EditText  nama, username, password, jenis_kelamin, alamat;
    Button btnsave, btnTampildata, btnupdate,btndelete;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_pegawai);


        nama = (EditText) findViewById(R.id.edt_nama);
        username = (EditText) findViewById(R.id.edt_username);
        password = (EditText) findViewById(R.id.edt_password);
        jenis_kelamin = (EditText) findViewById(R.id.edt_jeniskelamin);
        alamat = (EditText) findViewById(R.id.edt_alamat);
        btnTampildata = (Button) findViewById(R.id.btntampildata);
        btnupdate =(Button) findViewById(R.id.btnUpdate);
        btnsave = (Button) findViewById(R.id.btn_insertdata);
        btndelete=(Button) findViewById(R.id.btnhapus);

        Intent data = getIntent();
        final String id_pegawai = data.getStringExtra("id_pegawai");
        if(id_pegawai != null) {
            btnsave.setVisibility(View.GONE);
            btnTampildata.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
            nama.setText(data.getStringExtra("nama"));
            username.setText(data.getStringExtra("username"));
            password.setText(data.getStringExtra("password"));
            jenis_kelamin.setText(data.getStringExtra("jenis_kelamin"));
            alamat.setText(data.getStringExtra("alamat"));

        }

        pd = new ProgressDialog(this);



        btnTampildata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent godata = new Intent(TambahPegawai.this, TampilData2.class);
                startActivity(godata);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Loading Hapus ...");
                pd.setCancelable(false);
                pd.show();

                ApiRequest2 api = RetroServer2.getClient().create(ApiRequest2.class);
                Call<ResponsModel2> del  = api.deleteData(id_pegawai);
                del.enqueue(new Callback<ResponsModel2>() {
                    @Override
                    public void onResponse(Call<ResponsModel2> call, Response<ResponsModel2> response) {
                        Log.d("Retro", "onResponse");
                        Toast.makeText(TambahPegawai.this, response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        Intent gotampil = new Intent(TambahPegawai.this,TampilData2.class);
                        startActivity(gotampil);

                    }

                    @Override
                    public void onFailure(Call<ResponsModel2> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("update ....");
                pd.setCancelable(false);
                pd.show();

                ApiRequest2 api = RetroServer2.getClient().create(ApiRequest2.class);
                Call<ResponsModel2> update = api.updateData(id_pegawai,nama.getText().toString(),username.getText().toString(),password.getText().toString(),jenis_kelamin.getText().toString(),alamat.getText().toString());
                update.enqueue(new Callback<ResponsModel2>() {
                    @Override
                    public void onResponse(Call<ResponsModel2> call, Response<ResponsModel2> response) {
                        Log.d("Retro", "Response");
                        Toast.makeText(TambahPegawai.this,response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        pd.hide();
                    }

                    @Override
                    public void onFailure(Call<ResponsModel2> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "OnFailure");

                    }
                });
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("send data ... ");
                pd.setCancelable(false);
                pd.show();

                String snama = nama.getText().toString();
                String susername = username.getText().toString();
                String spassword = password.getText().toString();
                String sjenis_kelamin = jenis_kelamin.getText().toString();
                String salamat = alamat.getText().toString();

                ApiRequest2 api = RetroServer2.getClient().create(ApiRequest2.class);

                Call<ResponsModel2> sendbio = api.sendBiodata(snama,susername,spassword,sjenis_kelamin,salamat );
                sendbio.enqueue(new Callback<ResponsModel2>() {
                    @Override
                    public void onResponse(Call<ResponsModel2> call, Response<ResponsModel2> response) {
                        pd.hide();
                        Log.d("RETRO", "response : " + response.body().toString());
                        String kode = response.body().getKode();

                        if(kode.equals("1"))
                        {
                            Toast.makeText(TambahPegawai.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            Toast.makeText(TambahPegawai.this, "Data Error tidak berhasil disimpan", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsModel2> call, Throwable t) {
                        pd.hide();
                        Log.d("RETRO", "Falure : " + "Gagal Mengirim Request");
                    }
                });
            }
        });
    }
}
