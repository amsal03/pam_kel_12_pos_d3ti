package stud.develops.pointofsale.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import stud.develops.pointofsale.model.ResponsModel;


/**
 * Created by ASUS-PC on 14/05/2019.
 */

public interface ApiRequest {
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponsModel> sendBiodata(@Field("nama") String nama,
                                   @Field("harga") String harga);


    @GET("read.php")
    Call<ResponsModel> getBiodata();

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponsModel> updateData(@Field("id_makanan") String id_makanan,
                                  @Field("nama") String nama,
                                  @Field("harga") String harga);

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponsModel> deleteData(@Field("id_makanan") String id_makanan);
}
