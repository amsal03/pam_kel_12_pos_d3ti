package stud.develops.pointofsale;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterPegawai extends RecyclerView.Adapter<AdapterPegawai.PegawaiViewHolder> {
    private ArrayList<Pegawaim>dataList;

    public AdapterPegawai(ArrayList<Pegawaim> dataList){
        this.dataList = dataList;
    }

    @Override
    public AdapterPegawai.PegawaiViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_pegawai, parent, false);
        return new AdapterPegawai.PegawaiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterPegawai.PegawaiViewHolder holder, int position){
        holder.txt_nama.setText(dataList.get(position).getNama());
        holder.txt_username.setText(dataList.get(position).getUsername());
        holder.txt_password.setText(dataList.get(position).getPassword());
        holder.txt_jeniskelamin.setText(dataList.get(position).getJeniskelamin());
        holder.txt_alamat.setText(dataList.get(position).getAlamat());
    }

    @Override
    public int getItemCount(){
        return  (dataList != null) ? dataList.size():0;
    }

    public class PegawaiViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_nama, txt_username, txt_password, txt_jeniskelamin, txt_alamat;

        public PegawaiViewHolder (View itemView){
            super(itemView);
            txt_nama = (TextView) itemView.findViewById(R.id.txt_nama);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_password = (TextView) itemView.findViewById(R.id.txt_password);
            txt_jeniskelamin = (TextView) itemView.findViewById(R.id.txt_jeniskelamin);
            txt_alamat = (TextView) itemView.findViewById(R.id.txt_alamat);
        }
    }
}
