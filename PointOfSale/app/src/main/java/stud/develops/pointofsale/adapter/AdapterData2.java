package stud.develops.pointofsale.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import stud.develops.pointofsale.R;
import stud.develops.pointofsale.TambahPegawai;
import stud.develops.pointofsale.model.DataModel2;

public class AdapterData2 extends RecyclerView.Adapter<AdapterData2.HolderData> {

private List<DataModel2> mList ;
private Context ctx;


public  AdapterData2 (Context ctx, List<DataModel2> mList)
        {
        this.ctx = ctx;
        this.mList = mList;
        }

@Override
public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist2,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
        }

@Override
public void onBindViewHolder(HolderData holder, int position) {
    DataModel2 dm = mList.get(position);
        holder.nama.setText(dm.getNama());
        holder.username.setText(dm.getUsername());
        holder.password.setText(dm.getPassword());
        holder.jenis_kelamin.setText(dm.getJenis_kelamin());
        holder.alamat.setText(dm.getAlamat());

        holder.dm = dm;
        }

@Override
public int getItemCount() {
        return mList.size();
        }


class HolderData extends  RecyclerView.ViewHolder{
    TextView nama, username, password, jenis_kelamin, alamat;
    DataModel2 dm;
    public HolderData (View v)
    {
        super(v);

        nama  = (TextView) v.findViewById(R.id.tvNama);
        username = (TextView) v.findViewById(R.id.tvUsername);
        password  = (TextView) v.findViewById(R.id.tvPassword);
        jenis_kelamin = (TextView) v.findViewById(R.id.tvJenisKelamin);
        alamat = (TextView) v.findViewById(R.id.tvAlamat);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goInput = new Intent(ctx, TambahPegawai.class);
//                    goInput.putExtra("id_makanan", dm.getId_makanan());
                goInput.putExtra("nama", dm.getNama());
                goInput.putExtra("username", dm.getUsername());
                goInput.putExtra("password", dm.getPassword());
                goInput.putExtra("jenis_kelamin", dm.getJenis_kelamin());
                goInput.putExtra("alamat", dm.getAlamat());

                ctx.startActivity(goInput);
            }
        });
    }
}
}
