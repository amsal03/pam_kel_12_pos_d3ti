package stud.develops.pointofsale;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class MenuMakanan extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdapterMakanan adapterMakanan;
    private ArrayList<Makanan>makananArrayList;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_makanan);

//        fab=(FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MenuMakanan.this, TambahMakanan.class);
//                startActivity(intent);
//            }
//        });

        addData();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        adapterMakanan = new AdapterMakanan(makananArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MenuMakanan.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterMakanan);
    }

    void addData(){
        makananArrayList = new ArrayList<>();
        makananArrayList.add(new Makanan("Nasi Goreng", "10000", "10"));
        makananArrayList.add(new Makanan("Nasi Ayam", "10000", "10"));
        makananArrayList.add(new Makanan("Nasi Pecal", "10000", "10"));
        makananArrayList.add(new Makanan("Nasi Soto", "10000", "10"));
        makananArrayList.add(new Makanan("Nasi Uduk", "10000", "10"));
    }
}
