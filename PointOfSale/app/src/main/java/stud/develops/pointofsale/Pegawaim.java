package stud.develops.pointofsale;

public class Pegawaim {

    private String nama;
    private String username;
    private String password;
    private String jeniskelamin;
    private String alamat;

    public Pegawaim(String nama, String username, String password, String jeniskelamin, String alamat) {
        this.nama = nama;
        this.username = username;
        this.password = password;
        this.jeniskelamin = jeniskelamin;
        this.alamat = alamat;
    }

    public Pegawaim() {
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJeniskelamin() {
        return jeniskelamin;
    }

    public void setJeniskelamin(String jeniskelamin) {
        this.jeniskelamin = jeniskelamin;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
